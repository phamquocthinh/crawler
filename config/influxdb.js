const Util = require('../libs/utils')
const INFLUXDB_HOST = Util.getEnv('INFLUXDB_HOST', '142.44.215.112')
const INFLUXDB_PORT = Util.getEnv('INFLUXDB_PORT', 8086)
const INFLUXDB_DB = Util.getEnv('INFLUXDB_DB', 'mydb')

module.exports = {
    host: INFLUXDB_HOST,
    port: INFLUXDB_PORT,
    database: INFLUXDB_DB
};
