const Util = require('../libs/utils')
const PROXY_HOST = Util.getEnv('PROXY_HOST', 'localhost')
const PROXY_PORT = Util.getEnv('PROXY_PORT', 5000)

module.exports = {
    host: PROXY_HOST,
    port: PROXY_PORT
};
