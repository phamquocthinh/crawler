const mongoose = require('mongoose')
const config = require('../config/mongodb')

module.exports = {
    connect: async () => {
        const {
            host,
            port,
            db
        } = config
    
        const mongoUrl =  `mongodb://${host}:${port}/${db}`
        console.log(`Starting mongoose at ${mongoUrl}`)

        try {
            const conn = await mongoose.connect(mongoUrl)
    
            console.log(`Mongoose connected...`)
        
            process.on('SIGINT', () => {
                conn.connection.close(() => {
                    console.log('Mongoose default connection is disconnected due to application termination')
                    process.exit(0)
                })
            })
        
            return conn
        } catch(e) {
            console.log(e)
            process.exit(1)
        }
    }
}