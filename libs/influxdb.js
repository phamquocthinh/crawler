const Promise = require('bluebird') 
const Influx = require('influx')
const { host,port,database } = require('../config/influxdb')

const config = {
    host,
    port,
    database,
	requestTimeout: 60000
}

console.log(config)

const influx = new Influx.InfluxDB({
    host,
    port,
    database,
	requestTimeout: 60000
})

module.exports = {
	write: (measurement, tags, fields, time) => {
		return influx.writePoints([
            {
              measurement,
              tags,
              fields,
              time
            }
          ])
	},

	query: (query) => {
		return influx.query(query)
	}
}