
const request = require('request')
const _ = require('lodash')
const zlib = require('zlib')
const iconvlite = require('iconv-lite')
const autoBind = require('auto-bind')

const browserAgents = [
    'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.1 Safari/537.36',
    'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.0 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; AS; rv:11.0) like Gecko',
    'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; WOW64; Trident/6.0)',
    'Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.0',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10; rv:33.0) Gecko/20100101 Firefox/33.0'
]

class Downloader {
    constructor(service, options) {
        const defaults = {
            useProxy: true
        }
    
        this.settings = _.extend(defaults, options)
        this.service = service

        this.maxRetryTimes = 3

        this.ERROR_CODE_NO_PROXY = 0
        this.ERROR_CODE_MAX_TRIES = 1
        this.ERROR_CODE_EMPTY_RESPONSE = 2
        this.ERROR_CODE_NO_URL = 3
        this.ERROR_CODE_INVALID_BODY = 4
        this.ERROR_CODE_INVALID_REQUEST = 5
        this.ERROR_CODE_UNKNOWN = 6
        this.ERROR_CODE_INVALID_REDIRECT = 7

        autoBind(this)
    }

    requestWithEncoding(options, proxy) {
        let req

        req = proxy ? request.defaults({ proxy: proxy }) : request
        
        return new Promise((resolve, reject) => {
            req = req.get(options)

            req.on('response', (res) => {
                let chunks = []
    
                res.on('data', (chunk) => {
                    chunks.push(chunk)
                })
    
                res.on('end', () => {
                    let buffer = Buffer.concat(chunks)
                    let encoding = res.headers['content-encoding']
    
                    if (encoding == 'gzip') {
                        zlib.gunzip(buffer, (error, body) => {
                            if (error) {
                                return resolve({
                                    response: res,
                                    error_code: this.ERROR_CODE_INVALID_BODY,
                                    error_message: error
                                })
                            }
    
                            body = this.sanitizeBody(body, options)
    
                            resolve({
                                response: res,
                                body: body
                            })
                        })
                    } else if (encoding == 'deflate') {
                        zlib.inflateRaw(buffer, (error, body) => {
                            if (error) {
                                return resolve({
                                    response: res,
                                    error_code: this.ERROR_CODE_INVALID_BODY,
                                    error_message: error
                                })
                            }
    
                            body = this.sanitizeBody(body, options)
    
                            resolve({
                                response: res,
                                body: body
                            })
                        })
                    } else {
                        var body = this.sanitizeBody(buffer, options)
    
                        resolve({
                            response: res,
                            body: body
                        })
                    }
                })
            })
    
            req.on('error', (error) => {
                return reject({
                    response: null,
                    error_code: this.ERROR_CODE_INVALID_REQUEST,
                    error_message: error
                })
            })
        })
    }

    getProxy(item, settings) {
        if (!settings.useProxy) return Promise.resolve(false)

        return this.service.call(['ProxyManager', 'getProxy'], [item.site_name])
            .then((result) => {
                if (result.status == 'error') {
                    console.error(`Calling Proxy Manager service: ${result.message}`)
                    return false
                }

                return result.value
            })
            .catch((error) => {
                console.error(`Calling Proxy Manager service: ${error}`)
                return false
            })
    }

    getUrlContent(item, times, options) {
        times = times || 0

        return new Promise((resolve, reject) => {

            if (!item.url) {
                return resolve({
                    status: 'error',
                    error_code: this.ERROR_CODE_NO_URL,
                    error_message: 'No urls to request'
                })
            }

            this.getProxy(item, this.settings)
                .then((proxy) => {
                    if (this.settings.useProxy && !proxy) {
                        return resolve({
                            status: 'error',
                            error_code: this.ERROR_CODE_NO_PROXY,
                            error_message: 'Not found proxy to request'
                        })
                    }

                    console.log(`Request ${item.url} with Proxy ${proxy}`)

                    let defaultOpt = {
                        url: item.url,
                        headers: {
                            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
                            'User-Agent': _.sample(browserAgents),
                            'Accept-Encoding' : 'gzip,deflate'
                        },
                        maxRedirects: 10,
                        jar: true,
                        encoding: null,
                        rejectUnauthorized: false,
                        timeout: 20 * 1000 // 20 seconds
                    }

                    options = options || {}
                    options = _.extend(defaultOpt, options)

                    if (options.cookies) {
                        options.headers.Cookie = options.cookies
                        delete options.cookies
                    }

                    
                    this.requestWithEncoding(options, proxy)
                        .then(result => {
                            if (result.response && result.response.statusCode == 200) {
                                if (!result.body) {
                                    return resolve({
                                        status: 'error',
                                        error_code: this.ERROR_CODE_EMPTY_RESPONSE,
                                        error_message: 'Empty body response'
                                    })
                                }

                                return resolve({
                                    status: 'success',
                                    body: _.trim(result.body)
                                })
                            }
                            console.warn(`Request ${item.url} proxy ${proxy} failed! Status code: ${result.response.statusCode}`)
                            // Try again
                            if (this.settings.useProxy && (times < this.maxRetryTimes)) {
                                times++
                                console.warn(`Try download #${times} ${item.url}`)

                                return this.getUrlContent(item, times, options)
                                    .then(result => resolve(result))
                            }

                            return resolve({
                                status: 'error',
                                error_code: this.ERROR_CODE_MAX_TRIES,
                                error_message: `Reach max retry times with code ${result.response.statusCode}`
                            })
                        })
                        .catch(error => {
                            return resolve({
                                status: 'error',
                                error_code: this.ERROR_CODE_INVALID_REQUEST,
                                error_message: error
                            })
                        })
                })
            })
    }

    sanitizeBody(body, options) {
        if (options && options.decodeISO) {
            body = iconvlite.decode(body, 'iso-8859-1')
        }
    
        return body.toString('utf-8')
    }
}

module.exports=Downloader
