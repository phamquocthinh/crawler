
const dnode = require('dnode')

const config = require('../config/proxy')
const ServiceClient = (() => {
    return {
        /**
         * Call a service
         *
         * @param serviceInfo array[serviceName, serviceMethod]
         * @param args array arguments
         * @returns Promise
         */
        call: (serviceInfo, args) => {
            const node = dnode({},{weak: false}).connect(config.host, config.port)

            return new Promise((resolve, reject) => {
                node.on('remote', remote => {
                    remote.call(serviceInfo, args, result => {
                        node.end()
                        resolve(result)
                    })
                })

                node.on('fail', error => {
                    resolve({
                        status: 'error',
                        message: error.code
                    })
                })

                node.on('error', error => {
                    resolve({
                        status: 'error',
                        message: error.code
                    })
                })
            })
        }
    }
})()

module.exports = ServiceClient
