const Promise = require('bluebird')
const moment = require('moment')
const _ = require('lodash')
const autoBind = require('auto-bind')
const exec = require('child_process').exec

const browserAgents = [
    'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.1 Safari/537.36',
    'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.0 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; AS; rv:11.0) like Gecko',
    'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; WOW64; Trident/6.0)',
    'Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.0',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10; rv:33.0) Gecko/20100101 Firefox/33.0'
]

class Downloader {
    constructor(service, options) {
        const defaults = {
            useProxy: true
        }
    
        this.settings = _.extend(defaults, options)
        this.service = service

        this.maxRetryTimes = 3

        autoBind(this)
    }

    getProxy(item, settings) {
        if (!settings.useProxy) return Promise.resolve(false)

        return this.service.call(['ProxyManager', 'getProxy'], [item.site_name])
            .then((result) => {
                if (result.status == 'error') {
                    console.error(`Calling Proxy Manager service: ${result.message}`)
                    return false
                }

                return result.value
            })
            .catch((error) => {
                console.error(`Calling Proxy Manager service: ${error}`)
                return false
            })
    }

    getUrlContent(item, times, options) {
        return this.getProxy(item, this.settings)
            .then(proxy => {
                let browserAgent = _.sample(browserAgents)
                let cmd = "curl -sfL --compressed -H 'User-Agent: " + browserAgent + "' --max-time 10 "

                if (proxy) cmd += "-x '" + proxy + "' "

                cmd += "-L '" + item.url + "'"

                return new Promise((resolve, reject) => {
                    exec(cmd, {maxBuffer: 10*1024*1024}, (err, html) => {
                        if (err && !html) {
                            console.log(err)
                            return resolve()
                        }

                        return resolve(html)
                    })
                })
                .then((html) => {
                    if (!html) {
                        if (times < self.maxTimes && proxy) {
                            times++
                            return self.getUrlContent(item, times, options)
                        }

                        return {
                            status: 'error',
                            error_code: 8,
                            error_message: 'Curl error not found html'
                        }
                    }

                    return {
                        status: 'success',
                        body: html
                    }
                })
            })
            .catch((error) => {
                return Promise.resolve({
                    status: 'error',
                    error_code: 9,
                    error_message: JSON.stringify(error)
                })
            })
    }
}

module.exports=Downloader
