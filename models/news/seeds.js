const mongoose = require('mongoose')

const Schema = mongoose.Schema
const Seeds = new Schema({
	site_name: {
		type: String,
		required: true,
	},
	seed: {
		type: String,
		required: true,
	},
	status: String,
	updated_time: Date
}, { collection: 'news_seeds' })

module.exports = mongoose.model('newsSeeds', Seeds)