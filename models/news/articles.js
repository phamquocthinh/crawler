const mongoose = require('mongoose')

const Schema = mongoose.Schema
const Articles = new Schema({
  id: {
    type: String,
    required: true,
    unique: true
  },
  from: Schema.Types.Mixed,
  to: Schema.Types.Mixed,
  url: {
    type: String,
    required: true,
  },
  title: String,
  picture: String,
  message: String,
  contentRaw: String,
  description: String,
  created_time: Date,
  inserted_time: Date,
  delay_crawler_ms: Number,
  delay_mongo_ms: Number,
  ds: Schema.Types.Mixed,
  indexed: Number
}, { collection: 'news_articles' })

module.exports = mongoose.model('newsArticles', Articles)