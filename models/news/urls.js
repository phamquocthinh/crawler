const mongoose = require('mongoose')

const Schema = mongoose.Schema
const Urls = new Schema(
  {
    id: {
      type: String,
      required: true,
      unique: true
    },
    site_name: {
      type: String,
      required: true
    },
    url: {
      type: String,
      required: true
    },
    inserted_at: Date,
    last_processed_at: Date,
    processed: Number,
    isAddedToQueue: Boolean,
    ds: Schema.Types.Mixed,
    importedRow: Schema.Types.Mixed,
    data: Schema.Types.Mixed,
    error: Schema.Types.Mixed,
    error_code: Number,
    order: Number
  },
  { collection: 'news_urls' }
)

module.exports = mongoose.model('newsUrls', Urls)
