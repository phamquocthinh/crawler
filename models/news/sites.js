const mongoose = require('mongoose')

const Schema = mongoose.Schema
const Sites = new Schema({
	site_name: {
		type: String,
		required: true,
	},
	domain: {
		type: String,
		required: true,
	},
	seed_urls: Array,
	priority: Number,
    url_processor: Schema.Types.Mixed,
    article_processor: Schema.Types.Mixed,
    testing: Schema.Types.Mixed
}, { collection: 'news_sites' })

module.exports = mongoose.model('newsSites', Sites)