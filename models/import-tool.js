const mongoose = require('mongoose')

const Schema = mongoose.Schema
const ImportLinks = new Schema({
    url : String,
    user_id : Number,
    user_name : String,
    id : String,
    topic_ids : Array,
    is_followed : Boolean,
    order : Number,
    type_id : Number,
    type_name : String,
    inserted_at : Date,
    updated_at : Date,
    crawled: Number,
    crawled_at: Date,
    status_code: Number,
    status_message: String,
    result: Schema.Types.Mixed
}, { collection: 'import_links' })

module.exports = mongoose.model('importLinks', ImportLinks)