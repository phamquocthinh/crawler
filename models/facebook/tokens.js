const mongoose = require('mongoose')

const Schema = mongoose.Schema
const Tokens = new Schema({
	token: {
		type: String,
        required: true,
        unique: true
    },
    script: String,
    is_disabled: Boolean,
    groups: Schema.Types.Mixed
}, { collection: 'fb_tokens' })

module.exports = mongoose.model('fbTokens', Tokens)