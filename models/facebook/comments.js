const mongoose = require('mongoose')

const Schema = mongoose.Schema
const Comments = new Schema({
	id: {
		type: String,
        required: true,
        unique: true
    },
    from: Schema.Types.Mixed,
    parent : Schema.Types.Mixed,
    message : String,
    like_count : Number,
    comment_count : Number,
    created_time : Date,
    inserted_time : Date,
    to : String,
    post : Schema.Types.Mixed,
    commentParentId : String,
    cursor : String,
    delay_mongo_ms : Number,
    ds : Schema.Types.Mixed,
    indexed : Number
}, { collection: 'fb_comments' })

module.exports = mongoose.model('fbComments', Comments)