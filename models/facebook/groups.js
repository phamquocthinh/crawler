const mongoose = require('mongoose')

const Schema = mongoose.Schema
const Groups = new Schema({
	id: {
		type: String,
        required: true,
        unique: true
    },
    lastPost: Date
}, { collection: 'fb_groups' })

module.exports = mongoose.model('fbGroups', Groups)