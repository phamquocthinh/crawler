const mongoose = require('mongoose')

const Schema = mongoose.Schema
const Posts = new Schema({
	id: {
		type: String,
        required: true,
        unique: true
    },
    from: Schema.Types.Mixed,
    to: Schema.Types.Mixed,
    postType: String,
    message: String,
    name: String,
    description: String,
    caption: String,
    picture: String,
    created_time: Date,
    updated_time: Date,
    inserted_time: Date,
    likes: Number,
    comments: Number,
    shares: Number,
    delay_mongo_ms: Number,
    cursor: String,
    error: Schema.Types.Mixed,
    ds: Schema.Types.Mixed
}, { collection: 'fb_posts_restricted' })

module.exports = mongoose.model('fbPostsRestricted', Posts)