const rootPath = '../../..'

const Promise = require('bluebird')
const moment = require('moment')
const _ = require('lodash')

const MongoDb = require(rootPath + '/libs/mongodb')
const logger = require(rootPath + '/libs/logger')

const Tokens = require(rootPath + '/models/facebook/tokens')
const Groups = require(rootPath + '/models/facebook/groups')
const Posts = require(rootPath + '/models/facebook/posts_restricted')

const FB = require('../libs/fb')

const Util = require(rootPath + '/libs/utils')

const sleep = (ms) => {
    logger.info(`Sleeping for ${parseInt(ms/1000)} seconds`)
    return new Promise(resolve => setTimeout(resolve, ms))
}

const getTokens = () => {
    return Tokens.find({script: 'getPostsCustomGroups', is_disabled: {'$ne': true}})
}

const getGroups = (token) => {
    let condition = {
        id: {'$in': token.groups}, 
        disabled: {'$ne': 1}
    }

    return Groups.find(condition)
}

const getPosts = async(group, token) => {
    let fb = new FB(token)
    return await fb.getGroupFeed(group)
}

const execute = async() => {
    let tokens = await getTokens()
    let total = 0

    if (!tokens.length) {
        logger.info('Not found any tokens to process...')
        await sleep(5000)
        process.exit()
    }

    return Promise.each(tokens, async(token) => {
        let groups = await getGroups(token)

        if (!groups.length) {
            logger.info('Not found any groups to process...')
            await sleep(5000)
            process.exit()
        }

        return Promise.map(groups, async(group) => {
            let posts = await getPosts(group, token.token)

            if (!posts.length) {
                logger.info(`Found nothing new from ${group.id}`)
                return
            }

            logger.info(`Found ${posts.length} posts from group ${group.id}`)

            total += posts.length

            posts = _.map(posts, post => {
                post.inserted_time = moment().toDate()
                post.delay_mongo_ms = moment() - moment(post.created_time)
                post.ds = {
                    source: 'get-fb-posts-from-custom-groups',
                    ip: Util.getIpAddress()
                }
                
                return post
            })

            // let insertPosts = await Posts.insertMany(posts, { ordered: false }, (err) => {
            //     if (err && err.code === '11000') {}
            // })

            let insertPosts = await _.map(posts, async(post) => {
                await Posts.findOneAndUpdate({id: post.id}, post, {upsert: true})
            })

            let lastPostTime = posts[0].updated_time

            let updateLastPost = await Groups.updateOne({id: group.id}, {'$set': {
                lastPost: moment(lastPostTime).toDate()
            }})

            return Promise.all([insertPosts, updateLastPost])
        }, {concurrency: 4})
    }).then(async() => {
        logger.info(`Finished, found total ${total} posts`)
        await sleep(2 * 60 * 1000)
        process.exit()
    })
}

MongoDb.connect()

try {
    execute()
} catch(e) {
    logger.error(e)
    process.exit()
}