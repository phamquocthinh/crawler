const rootPath = '../../..'

const Promise = require('bluebird')
const moment = require('moment')
const _ = require('lodash')

const MongoDb = require(rootPath + '/libs/mongodb')
const logger = require(rootPath + '/libs/logger')

const Tokens = require(rootPath + '/models/facebook/tokens')
const Comments = require(rootPath + '/models/facebook/comments')
const Posts = require(rootPath + '/models/facebook/posts_restricted')

const FB = require('../libs/fb')

const Util = require(rootPath + '/libs/utils')

const clusterName = 'getCommentsCustomGroups'
const batch = 500

const sleep = (ms) => {
    logger.info(`Sleeping for ${parseInt(ms/1000)} seconds`)
    return new Promise(resolve => setTimeout(resolve, ms))
}

const getTokens = () => {
    return Tokens.find({script: clusterName, is_disabled: {'$ne': true}})
}

const getPosts = (token, lastTime) => {
    let condition = { 'to.id': {'$in': token.groups} }

    if (lastTime) {
        condition['updated_time'] = { $lt: moment(lastTime).toDate(), $gt: moment().subtract(7, 'days').toDate() }
    } else {
        condition['updated_time'] = { $gt: moment().subtract(7, 'days').toDate() }
    }

    condition.postType = { $in: ['page', 'group'] }
    condition.error = { $exists: false }

    return Posts.find(condition).sort({updated_time: -1}).limit(batch)
}

const getComments = async(post, token) => {
    let fb = new FB(token)
    return await fb.getComments(post)
}

const start = async(token, lastTime) => {
    let posts = await getPosts(token, lastTime)

    if (!posts.length) {
        //logger.info('Not found any posts to process...')
        return
    }

    logger.info(`Getting comments of ${posts.length} posts from ${moment(lastTime).format('DD/MM/YYYY HH:mm')}`)
    
    lastTime = posts[posts.length - 1].updated_time

    return Promise.map(posts, async(post) => {
        let comments = []
        
        try {
            comments = await getComments(post, token.token)
        } catch(e) {
            if (e && e.status === 100) {
                await Posts.updateOne({id: post.id}, {'$set': {error: e.body.error}} )
            }
        }
        
        if (!comments.length) {
            //logger.info(`Found nothing new from ${post.id}`)
            return
        }

        //logger.info(`Found ${comments.length} comments from post ${post.id}`)

        total += comments.length

        comments = _.map(comments, comment => {
            comment.inserted_time = moment().toDate()
            comment.delay_mongo_ms = moment() - moment(comment.created_time)
            comment.ds = {
                source: 'get-fb-comments-from-custom-groups',
                ip: Util.getIpAddress()
            }

            return comment
        })

        let insertComments = await Comments.insertMany(comments, { ordered: false }, (err) => {
            if (err && err.code === '11000') {}
        })

        let cursor = comments[comments.length - 1].cursor
        let lastCommentTime = comments[comments.length - 1].created_time

        let updateCursor = await Posts.updateOne({id: post.id}, {'$set': {
            cursor,
            updated_time: lastCommentTime
        }})

        return Promise.all([insertComments, updateCursor])
    }, {concurrency: 4})
    .then(() => {
        return start(token, lastTime)
    })
}

const execute = async() => {
    let tokens = await getTokens()

    if (!tokens.length) {
        logger.info('Not found any tokens to process...')
        await sleep(60 * 1000)
        process.exit()
    }

    return Promise.each(tokens, async(token) => {
        let lastTime = moment().toDate()

        return start(token, lastTime)
    }).then(async() => {
        logger.info(`Finished, found total ${total} comments`)
        await sleep(5 * 60 * 1000)
        process.exit()
    })
}

let total = 0
MongoDb.connect()

try {
    execute()
} catch(e) {
    logger.error(e)
    process.exit()
}