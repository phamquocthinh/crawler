const Promise = require('bluebird')
const moment = require('moment')
const _ = require('lodash')
const autoBind = require('auto-bind')
const request = require('request')

const postFields = `id,from,to,type,status_type,actions,message,link,name,caption,description,picture,created_time,likes.limit(0).summary(total_count),shares`
const commentFields = 'id,from,parent,message,like_count,comment_count,created_time'
const postLimit = 100
const commentLimit = 500

class FB {
    constructor(token) {
        this.token = token
        autoBind(this)
    }

    query(path) {
        return new Promise((resolve, reject) => {
            let url = 'https://graph.facebook.com' + path + '&access_token=' + this.token
            // url = encodeURI(url)

            request(url, (err, response, body) => {
                if (err) {
                    return reject(err)
                }

                let bodyJson = { body: body }

                try {
                    bodyJson = JSON.parse(body)
                } catch (e) {
                    return reject(e)
                }

                if (response.statusCode !== 200) {
                    let status = bodyJson.error && bodyJson.error.code ? bodyJson.error.code : response.statusCode

                    return reject({
                        status: status,
                        body: bodyJson,
                        token: this.token
                    })
                }

                resolve(bodyJson)
            })
        })
    }

    getGroupFeed(item) {
        let since = ''

        if (item.lastPost) {
            since = '&since=' + moment(item.lastPost).format('X')
        } else {
            let aMonthAgo = moment().subtract(30, 'days').format('X')
            since = '&since=' + aMonthAgo
        }

        return this.query(`/${item.id}/feed?include_hidden=true&fields=${postFields}${since}&limit=${postLimit}`)
            .then((json) => {
                return this._convertPostsToModels(json.data, { type: 'group' })
            })
            .catch((error) => {
                return Promise.reject(error)
            })
    }

    _convertPostsToModels(posts, options) {
        let { type } = options

        return _.map(posts, post => {
            if (post.id.indexOf('_') === -1) {
                post.id = post.from.id + '_' + post.id
            }

            let sourceData = post.to
            let fromId = post.id.split('_')[0]

            post.to = post.from

            let actionsField = post.actions
            let isGroupPost = false

            if (actionsField && actionsField.length) {
                let lastAction = actionsField[actionsField.length - 1]
                if ((lastAction.name && lastAction.name.indexOf('Join') > -1) || (lastAction.link && lastAction.link.indexOf('https://www.facebook.com/groups') > -1 )) {
                    isGroupPost = true
                }
            }

            if (type) {
                post.postType = type
            } else if ((post.from && post.from.category) || (post.status_type === 'wall_post')) {
                post.postType = 'page'
            } else if (isGroupPost) {
                post.postType = 'group'
            } else {
                post.postType = 'user'
            }

            if (post.from && ((fromId !== post.from.id) || post.postType === 'group') && sourceData) {
                post.to = sourceData.data[0]
            }

            post.likes = post.likes ? (post.likes.summary ? post.likes.summary.total_count : post.likes.count) : 0
            post.comments = post.comments ? (post.comments.summary ? post.comments.summary.total_count : post.comments.count) : 0
            post.shares = post.shares ? post.shares.count : 0

            post.created_time = moment(post.created_time).toDate()
            post.updated_time = post.updated_time ? moment(post.updated_time).toDate() : moment(post.created_time).toDate()

            return post
        })
    }

    getComments(post) {
        let after = ''

        if(post.cursor) {
            after = '&after=' + post.cursor
        }

        let postId = post.id

        if (postId.indexOf('_') === -1) {
            return Promise.resolve([])
        }

        postId = postId.split('_')[1]

        return this.query(`/v3.0/${post.id}/comments?filter=stream&fields=${commentFields}${after}&limit=${commentLimit}`)
            .then((json) => {
                let comments = _.map(json.data, function(comment) {
                    if (comment.id.indexOf('_') === -1) {
                        comment.id = postId + '_' + comment.id
                    }

                    comment.to = post.id
                    comment.post = post
                    comment.created_time = moment(comment.created_time).toDate()

                    // Replies comments
                    if (comment.parent && comment.parent.id && comment.parent.id.indexOf('_') === -1) {
                        let commentParentId = postId + '_' + comment.parent.id
                        comment.commentParentId = commentParentId
                    }

                    return comment
                })

                if (json.paging) {
                    comments[comments.length - 1].cursor = json.paging.cursors.after
                }

                return comments
            })
            .catch((error) => {
                return Promise.reject(error)
            })
    }
}

module.exports=FB
