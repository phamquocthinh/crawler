const rootPath = '../../..'

const Uri = require('URIjs')
const Crypto = require('crypto')
const _ = require('lodash')
const cheerio = require('cheerio')
const moment = require('moment')
const stripTags = require('striptags')
const extractor = require('unfluff')
const chrono = require('chrono-node')

const normalizeHref = (domain, href) => {
    href = _.trimEnd(_.trim(href), '/')

    let uri = new Uri(href)
    if (uri.host()) {
        return href
    }

    domain = _.trimEnd(domain, '/')
    return domain + '/' + _.trimStart(href, '/')
}

const parseArticleUrls = (site, html) => {
    let { 
        url_processor,
        domain,
        site_name
    } = site

    let $ = cheerio.load(html)
    let urls = []
    let links = $('a').toArray()

    if (links.length) {
        urls = _.map(links, link => {
            let href = _.trim($(link).attr('href'))
            if (!href) return

            let url = normalizeHref(domain, href)

            if (url_processor.replace && url_processor.replace.from && url.match(url_processor.replace.from)) {
                url = url.replace(new RegExp(url_processor.replace.from), url_processor.replace.to)
            }

            if(!url.match(new RegExp(url_processor.pattern_match, 'i'))) return
            
            return url
        })
    }

    urls = _.uniq(_.compact(urls))

    return urls
}

const getUrlId = (url) => {
    return Crypto.createHash('md5').update(url, 'binary').digest('hex')
}

const getArticleId = (url, article) => {
    let uri = new Uri(url)

    return Crypto.createHash('md5').update(uri.domain() + article.title + url, 'binary').digest('hex')
}

const validArticleData = (data, url) => {
    let requiredFields = ['title', 'text', 'created_time']
    let isValid = true
    _.forEach(requiredFields, field => {
        if (!data[field]) {
            console.log(`Parse error: ${field.toUpperCase()} empty - ${url}`)
            isValid = false
        }
    })

    return isValid
}

const parseImageUrl = (domain, $) => {
    let url = $('meta[property="og:image"], meta[property="og:image:url"]').attr('content')
    let pattern_match = /^(.*?)(.jpg|.jpeg|.gif|.bmp|.png)(.*?)$/i

    if (!url || !url.match(pattern_match)) return ''

    url = normalizeHref(domain, url)
    url = url.replace(new RegExp(pattern_match), '$1$2')

    return url
}

const getFieldSelector = (field) => {
    let fieldObj = {}

    if ((field !== null) && (typeof field === 'object')) {
        fieldObj = field
    } else {
        fieldObj.selector = field
    }

    return fieldObj
}

const parseFieldText = ($, field) => {
    let text = ''
    let fieldSelector = getFieldSelector(field)

    if (fieldSelector.selector) {
        var $field = $(fieldSelector.selector).eq(0)

        if ($field.length) {
            // Remove script & style tags
            $field.find('script').remove()
            $field.find('style').remove()

            if (fieldSelector.remove) {
                $field.find(fieldSelector.remove).remove()
            }

            if (fieldSelector.attribute) {
                text = $field.attr(fieldSelector.attribute)
            } else {
                text = $field.html()
                text = stripTags(text, ['br', 'p'])
                text = stripTags(text, [], '\n')

                text = text.replace(/\t/g, '')
                text = text.replace(/((\r)*\n\s*)+/g, '\n')
                text = text.replace(/\s{2,}/g, ' ')

                text = _.trim(_.unescape(text))
            }
        }

        if (field.replaces && field.replaces.length) {
            _.each(field.replaces, config => {
                if (config.from && config.to) {
                    if (!config.isExpression && !config.isRegx) {
                        text = _.replace(text, new RegExp(config.from, 'g'), config.to)
                    }

                    if (config.isRegx && text.match(config.from)) {
                        text = text.replace(new RegExp(config.from), config.to)
                    }

                    if (config.isExpression) {
                        text = _.replace(text, new RegExp(config.from, 'g'), eval(config.to))
                    }
                }
            })
        }
    }

    return text
}

const parseArticle = (articleProcessor, siteName, domain, url, html) => {
    let $ = cheerio.load(html, {
        normalizeWhitespace: false,
        decodeEntities: false
    })

    let data = {}

    _.forEach(articleProcessor, (field, key) => {
        data[key] = parseFieldText($, field)

        // Normalize data
        switch (key) {
            case 'created_time':
                let dateTime
                let matches

                if ((data[key].indexOf('AM') > -1) || (data[key].indexOf('PM') > -1)) {
                    if (field.format.indexOf('A') === -1) {
                        field.format += ' A'
                    }
                }

                if (matches = data[key].match(/(\d+) (seconds?|minutes?|hours?|days?|weeks?|months?|years?)/)) {
                    dateTime = moment().subtract(parseInt(matches[1]), matches[2])
                    if (dateTime.isValid()) {
                        data[key] = dateTime.toDate()
                    } else {
                        data[key] = ''
                    }
                } else {
                    dateTime = moment(data[key], field.format)

                    if (field.timezone) {
                        dateTime = moment(dateTime).add(7 - parseInt(field.timezone), 'hours')
                    }

                    if (dateTime.isValid()) {
                        data[key] = dateTime.toDate()
                    } else {
                        data[key] = ''
                    }
                }

                break
            case 'picture':
                var pattern_match = '^(.*?)(.jpg|.jpeg|.gif|.bmp|.png)(.*?)$'

                if (data[key] && data[key].match(pattern_match)) {
                    data[key] = normalizeHref(domain, data[key])
                    data[key] = data[key].replace(new RegExp(pattern_match), '$1$2')
                } else {
                    data[key] = '' 
                }
                
                break
            case 'text' :
                let content_selector = null;
                if(!articleProcessor[key].selector){
                    content_selector = articleProcessor[key];
                }else{
                    content_selector = articleProcessor[key].selector;
                }
                data.contentRaw = $.html(content_selector);
                break;
        }
    })

    if (!validArticleData(data, url)) {
        return {
            status: 'error',
            data: data
        }
    }

    let article = {
        id: getArticleId(url, data),
        from: {
            id: data.author || siteName,
            name: data.author || siteName
        },
        to: {
            id: siteName,
            name: siteName
        },
        title: data.title,
        description: data.description || '',
        contentRaw: data.contentRaw || '',
        picture: data.picture || parseImageUrl(domain, $),
        message: data.text,
        created_time: data.created_time,
        url
    }

    return article
}

const parseArticleWithoutConfig = (html, options) => {
    let { url, siteName } = options
    let data = extractor(html, 'vi')

    let dateTime = moment(chrono.parseDate(data.date))

    if (dateTime.isValid()) {
        data['created_time'] = dateTime.toDate()
    } else {
        data['created_time'] = ''
    }

    if (!validArticleData(data, url)) {
        return {
            status: 'error',
            data: data
        }
    }

    let article = {
        id: getArticleId(url, data),
        from: {
            id: siteName,
            name: siteName
        },
        to: {
            id: siteName,
            name: siteName
        },
        title: data.title,
        description: data.description || '',
        contentRaw: data.contentRaw || '',
        picture: data.image || '',
        message: data.text,
        created_time: new Date(data.created_time),
        url
    }

    return article
}

module.exports = {
    parseArticleUrls,
    getUrlId,
    parseArticle,
    parseArticleWithoutConfig
}
