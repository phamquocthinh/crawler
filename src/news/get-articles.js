'use strict'

const _ = require('lodash')
const moment = require('moment')
const Promise = require('bluebird')

const rootPath = '../..'
const MongoDb = require(rootPath + '/libs/mongodb')

const Urls = require(rootPath + '/models/news/urls')
const Sites = require(rootPath + '/models/news/sites')
const Articles = require(rootPath + '/models/news/articles')

const QueueMaster = require(rootPath + '/libs/queue/master')

process.on('uncaughtException', function(error) {
    console.log(error)
    process.exit()
})

class ArticleMaster extends QueueMaster {
    constructor(channel, options) {
        super(channel, options)
        this.totalUrlsAdded = 0
        this.limitUrls = 5000
        this.limitInactiveJobs = 100
    }

    getUrls() {
        let conditions = {
            processed: { $ne: 1 },
            isAddedToQueue: { $ne: true }
        }

        return Urls.find(conditions).limit(100)
    }

    addUrl(data) {
        return this.addJob(data)
            .then(job => {
                job.on('failed attempt', () => {
                    job.remove(rmError => {
                        if (rmError) {
                            console.log(rmError)
                        }
                    })
                })

                job.on('failed', () => {
                    job.remove(rmError => {
                        if (rmError) {
                            console.log(rmError)
                        }
                    })
                })

                return Promise.resolve()
            }).catch(error => Promise.reject(error))
    }

    async start() {
        let urls = await this.getUrls()

        return Promise.map(urls, async(item) => {
            await Urls.update({ id: item.id }, { isAddedToQueue: true })
            let {
                id,
                site_name: siteName,
                url,
                inserted_at: insertedAt,
                importedRow
            } = item

            return this.addUrl({title: siteName, siteName, url, insertedAt, importedRow, id})
        }).then(() => {
            if (this.totalUrlsAdded > this.limitUrls) {
                return this.exitProcess(0)
            }

            return Promise.resolve()
        }).catch(error => Promise.reject(error))
    }

    async execute() {
        this.startProfiling()

        if (await this.canAddJobs(this.limitInactiveJobs)) {
            await this.start()
            await this.sleep(2000)
            
            return this.execute()
        }
        await this.sleep(5 * 1000)

        return this.execute()
    }
}

const channel = 'get-articles'
const options = {
    attempts: 1,
    profiling: {
        enable: true,
        jobs: 100,
        type: 'articles'
    }
}
const articleMaster = new ArticleMaster(channel, options)

MongoDb.connect()
    .then(articleMaster.execute())
