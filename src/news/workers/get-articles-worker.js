'use strict'

const rootPath = '../../..'

const Promise = require('bluebird')
const moment = require('moment')
const _ = require('lodash')

const logger = require(rootPath + '/libs/logger')
const MongoDb = require(rootPath + '/libs/mongodb')
const Influx = require(rootPath + '/libs/influxdb')

const Service = require(rootPath + '/libs/proxyClient')
const Downloader = require(rootPath + '/libs/downloader')
const downloader = new Downloader(Service)
const Curl = require(rootPath + '/libs/curl')
const curl = new Curl(Service)

const { parseArticle, parseArticleWithoutConfig } = require('../libs/parser')

const Urls = require(rootPath + '/models/news/urls')
const Sites = require(rootPath + '/models/news/sites')
const Articles = require(rootPath + '/models/news/articles')
const Imports = require(rootPath + '/models/import-tool')

const Util = require(rootPath + '/libs/utils')

const QueueWorker = require(rootPath + '/libs/queue/worker')

process.on('uncaughtException', function (error) {
    console.log(error)
    process.exit()
})

class ArticleWorker extends QueueWorker {
    constructor(channel, concurrency) {
        super(channel, concurrency)
    }

    getSite(siteName) {
        return Sites.findOne({ site_name: siteName, article_processor: {$exists: true} })
    }

    async process(item) {
        let {
            id,
            url,
            siteName,
            insertedAt,
            importedRow
        } = item
        let tmpUrl= url;
        var listSite=["dntm.vn", "dientutieudung.vn", "cand.com.vn", "nguoilambaotiengiang.vn", "vnca.cand.com.vn"];

        for (let i=0 ; i < listSite.length ; i++){
            if (siteName === listSite[i]) {
                tmpUrl = tmpUrl + '/' ;              
            }
        }  

        let response = await downloader.getUrlContent({         
            url: tmpUrl,
            site_name: siteName
        })

        if (response.status === 'error' && response.error_code === 5) {
            response = await curl.getUrlContent({
                url: tmpUrl,
                site_name: siteName
            })
        }

        if (response.status === 'success' && response.body) {
            let html = response.body
            let siteConfig = await this.getSite(siteName)
            let articleData

            if (!siteConfig) {
                let options = { url, siteName }
                articleData = parseArticleWithoutConfig(html, options)
            } else {
                let { article_processor: articleProcessor, domain } = siteConfig
                articleData = parseArticle(articleProcessor, siteName, domain, url, html)
            }
    
            if (!articleData.status) {
                logger.info(`Parse success - ${url}`)

                articleData.inserted_time = moment().toDate()
                articleData.delay_crawler_ms = moment() - moment(insertedAt)
                articleData.delay_mongo_ms = moment() - moment(articleData.created_time)
                articleData.ds = {
                    source: this.channel,
                    ip: Util.getIpAddress()
                }

                let saveMetric =  Influx.write('get_success', {type: 'article_news', domain: siteName}, {value: 1}, new Date())
                
                let saveArticle =  Articles.insertMany([articleData], {ordered: false}, err => {
                    if (err && err.code === '11000') {}
                })
                
                let updateUrl =  Urls.findOneAndUpdate(
                    { id: id },
                    { 
                        '$set': {
                            processed: 1, last_processed_at: moment().toDate() 
                        },
                        '$unset': {
                            error: 1, data: 1, code_download: 1, error_code_download: 1
                        }
                    }
                )

                let updateImportTool = importedRow ? Imports.findByIdAndUpdate(
                    importedRow._id,
                    { crawled: 1, crawled_at: moment().toDate(), status_code: 200, status_message: 'success', result: JSON.stringify(articleData)}
                ) : Promise.resolve()

                return Promise.all([saveMetric, saveArticle, updateUrl, updateImportTool])
            } else {
                let saveMetric = Influx.write('parse_error', {type: 'article_news', domain: siteName}, {value: 1}, new Date())

                let updateUrl =  Urls.findOneAndUpdate(
                    { id: id },
                    { processed: 1, error: 'parse_error', data: articleData.data, last_processed_at: moment().toDate() }
                )

                let updateImportTool = importedRow ? Imports.findByIdAndUpdate(
                    importedRow._id,
                    { crawled: 1, crawled_at: moment().toDate(), status_message: 'error', result: JSON.stringify(articleData.data) }
                ) : Promise.resolve()

                return Promise.all([saveMetric, updateUrl, updateImportTool])
            }
        } else {
            let saveMetric = Influx.write('get_error', {type: 'article_news', domain: siteName}, {value: 1}, new Date())

            logger.info(`Download ${url} error: ${JSON.stringify(response.error_message)} - code: ${response.error_code}`)

            let updateUrl =  Urls.findOneAndUpdate(
                { id: id },
                { processed: 1, error: response.error_message, error_code: response.error_code, last_processed_at: moment().toDate()}
            )

            let updateImportTool = importedRow ? Imports.findByIdAndUpdate(
                importedRow._id,
                { crawled: 1, crawled_at: moment().toDate(), status_message: 'error', status_code: response.error_code, result: response.error_message }
            ) : Promise.resolve()

            return Promise.all([saveMetric, updateImportTool, updateUrl])
        }
    }

    async callback(job, done) {
        let item = job.data

        try {
            await this.process(item)
            done()
        } catch(e) {
            logger.error(e)
            done(e)
        }
    }
}

const channel = 'get-articles'
const concurrency = 20
const articleWorker = new ArticleWorker(channel, concurrency)

MongoDb.connect()

try {
    articleWorker.execute(articleWorker.callback)
} catch(e) {
    console.log(e)
}
