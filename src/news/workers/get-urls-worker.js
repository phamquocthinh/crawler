'use strict'

const rootPath = '../../..'

const Promise = require('bluebird')
const moment = require('moment')
const _ = require('lodash')

const logger = require(rootPath + '/libs/logger')
const MongoDb = require(rootPath + '/libs/mongodb')
const Influx = require(rootPath + '/libs/influxdb')

const Service = require(rootPath + '/libs/proxyClient')
const Downloader = require(rootPath + '/libs/downloader')
const downloader = new Downloader(Service)
const Curl = require(rootPath + '/libs/curl')
const curl = new Curl(Service)

const { parseArticleUrls, getUrlId } = require('../libs/parser')

const Urls = require(rootPath + '/models/news/urls')
const Seeds = require(rootPath + '/models/news/seeds')

const Util = require(rootPath + '/libs/utils')

const QueueWorker = require(rootPath + '/libs/queue/worker')

process.on('uncaughtException', function(error) {
    console.log(error)
    process.exit()
})

class SeedWorker extends QueueWorker {
    constructor(channel, concurrency) {
        super(channel, concurrency)
    }

    async process(item) {
        let {
            site_name,
            url
        } = item

        let response = await downloader.getUrlContent({
            url,
            site_name
        })

        if (response && response.error_code === 5) {
            response = await curl.getUrlContent({
                url,
                site_name
            })
        }

        if (response.status === 'success' && response.body) {
            let articleUrls = parseArticleUrls(item, response.body)

            if (articleUrls.length) {
                logger.info(`Found ${articleUrls.length} urls from ${url}`)
                let saveMetric = Influx.write('get_success', {type: 'seed_news', domain: site_name}, {value: 1}, new Date())
                let updateSeed = Seeds.findOneAndUpdate({ seed: url }, { status: 'get_success', updated_time: moment().toDate() }, (err, doc) => {})

                articleUrls = _.map(articleUrls, u => {
                    if (site_name === 'antg.cand.com.vn') {
                        u = _.trimEnd(u, '/') + '/'
                    }

                    return {
                        id: getUrlId(u),
                        url: u,
                        site_name,
                        inserted_at: new Date(),
                        processed: 0,
                        ds: {
                            source: this.channel,
                            ip: Util.getIpAddress()
                        }
                    }
                })

                let saveUrl = Urls.insertMany(articleUrls, { ordered: false }, err => {
                    if (err.code === '11000') {}
                })

                return Promise.all([saveMetric, updateSeed, saveUrl])
            } else {
                return Promise.all([
                    Influx.write('parse_error', {type: 'seed_news', domain: site_name}, {value: 1}, new Date()),
                    Seeds.findOneAndUpdate({ seed: url }, { status: 'parse_error', updated_time: moment().toDate() }, (err, doc) => {})
                ])
            }
        } else {
            return Promise.all([
                Influx.write('get_error', {type: 'seed_news', domain: site_name}, {value: 1}, new Date()),
                Seeds.update({ seed: url }, { status: 'get_error', updated_time: moment().toDate() }, (err, doc) => {})
            ])
        }
    }

    async callback(job, done) {
        let item = job.data

        try {
            await this.process(item)
            done()
        } catch(e) {
            logger.error(e)
            done(e)
        }
    }
}

const channel = 'get-seed-urls'
const concurrency = 20
const seedWorker = new SeedWorker(channel, concurrency)

MongoDb.connect()

try {
    seedWorker.execute(seedWorker.callback)
} catch(e) {
    console.log(e)
}