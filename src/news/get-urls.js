'use strict'

const _ = require('lodash')
const moment = require('moment')
const Promise = require('bluebird')

const rootPath = '../..'
const MongoDb = require(rootPath + '/libs/mongodb')

const Urls = require(rootPath + '/models/news/urls')
const Sites = require(rootPath + '/models/news/sites')

const QueueMaster = require(rootPath + '/libs/queue/master')

process.on('uncaughtException', (error) => {
    console.log(error)
    process.exit()
})

class SeedMaster extends QueueMaster {
    constructor(channel, options) {
        super(channel, options)
        this.totalSeedAdded = 0
        this.limitSeeds = 50000
        this.limitInactiveJobs = 1000
    }

    getSites() {
        return Sites.find({ is_disabled: { $ne: true } })
    }

    addSeed(data) {
        return this.addJob(data)
            .then(job => {
                job.on('failed attempt', () => {
                    job.remove(rmError => {
                        if (rmError) {
                            console.log(rmError)
                        }
                    })
                })

                job.on('failed', () => {
                    job.remove(rmError => {
                        if (rmError) {
                            console.log(rmError)
                        }
                    })
                })

                return Promise.resolve()
            }).catch(error => Promise.reject(error))
    }

    async start() {
        let sites = await this.getSites()
        
        return Promise.map(sites, site => {
            let {
                last_request_time,
                period,
                seed_urls,
                site_name,
                domain,
                url_processor
            } = site

            let diff = moment().diff(moment(last_request_time), 'minutes')

            if (Math.abs(diff) < period) {
                return Promise.resolve()
            }

            this.totalSeedAdded += seed_urls.length

            return Promise.map(seed_urls, url => {
                let data = {
                    title: site_name,
                    site_name,
                    domain,
                    url_processor,
                    url
                }

                return this.addSeed(data)
            })
        }).then(() => {
            if (this.totalSeedAdded > this.limitSeeds) {
                return this.exitProcess(0)
            }

            return Promise.resolve()
        }).catch(error => Promise.reject(error))
    }

    async execute() {
        this.startProfiling()

        if (await this.canAddJobs(this.limitInactiveJobs)) {
            return this.start()
                .then(() => {
                    return this.execute()
                })
        }
        await this.sleep(5 * 1000)

        return this.execute()
    }
}

const channel = 'get-seed-urls'
const options = {
    attempts: 1,
    ttl: 1000 * 60 * 3,
    profiling: {
        enable: true,
        jobs: 1000,
        type: 'seeds'
    }
}
const seedMaster = new SeedMaster(channel, options)

MongoDb.connect()
    .then(seedMaster.execute())
